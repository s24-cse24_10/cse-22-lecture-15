#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <ucm_random>

/*  1.
    Print "Hello World!" n times.

    Example:

    Hello World!
    Hello World!
    Hello World!
*/

void printHelloWorld(int n) {
    for(int i = 0; i < n; i++) {
        std::cout << "Hello World!" << std::endl;
    }
}



/* 2.
    Ask user for message and display a banner.

    Example:

    =========
    UC Merced
    =========
*/

void printBanner(std::string message) {
    int length = message.length();

    for (int i = 0; i < length; i++) {
        std::cout << "=";
    }
    std::cout << std::endl;

    std::cout << message << std::endl;

    for (int i = 0; i < length; i++) {
        std::cout << "=";
    }
    std::cout << std::endl;    
}

/*  3.
    Ask user for message and display a padded banner.
    The padding should be 1 character.

    Example:

    ===========
     UC Merced 
    ===========
*/

void printPaddedBanner(std::string message) {
    int length = message.length() + 6;

    for (int i = 0; i < length; i++) {
        std::cout << "=";
    }
    std::cout << std::endl;

    std::cout << "   " << message << std::endl;

    for (int i = 0; i < length; i++) {
        std::cout << "=";
    }
    std::cout << std::endl;
}

/*  4.
    Ask user for message and display a fixed width banner.
    The width should always be 50 characters.

    Example:

    ==================================================
    | UC Merced                                      |
    ==================================================

*/

void printFixedBanner(std::string message) {
    for (int i = 0; i < 50; i++) {
        std::cout << "=";
    }
    std::cout << std::endl;

    std::cout << "| " << message;

    int spaces = 50 - message.length() - 3;
    for (int i = 0; i < spaces; i++) {
        std::cout << " ";
    }
    std::cout << "|" << std::endl;

    for (int i = 0; i < 50; i++) {
        std::cout << "=";
    }
    std::cout << std::endl;
}

/* 5.
    Ask user to input 3 integers.
    Display each input.

    Example:
    You entered: 3
    You entered: 5
    You entered: 7
*/

void inputNumbers(int n) {
    int x;

    for (int i = 0; i < n; i++) {
        std::cin >> x;
        std::cout << "You entered: " << x << std::endl;
    }    
}

/*  6.
    Generate n random numbers between 100 and 1000.

    Example:
    237
    649
    334
    719
    925
*/

void randomNumbers(int n) {
    RNG generator;

    for (int i = 0; i < n; i++) {
        int x = generator.get(100, 1000);
        std::cout << x << std::endl;
    }
}


#endif